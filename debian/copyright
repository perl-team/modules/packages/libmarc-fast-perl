Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: MARC-Fast
Upstream-Contact: Dobrica Pavlinusic <pavlin@rot13.org>
Source: https://metacpan.org/release/MARC-Fast

Files: *
Copyright: 2005-2013, Dobrica Pavlinusic <dpavlin@rot13.org>
License: Artistic or GPL-1+
Comment: The upstream distribution does not contain an explicit statement of
 copyright ownership. Pursuant to the Berne Convention for the Protection of
 Literary and Artistic Works, it is assumed that all content is copyright by
 its respective authors unless otherwise stated.
 .
 Copyright years are taken from the Changes file.

Files: debian/*
Copyright: 2023, Mason James <mtj@kohaaloha.com>
License: Artistic or GPL-1+

License: Artistic
 This program is free software; you can redistribute it and/or modify
 it under the terms of the Artistic License, which comes with Perl.
 .
 On Debian systems, the complete text of the Artistic License can be
 found in `/usr/share/common-licenses/Artistic'.

License: GPL-1+
 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 1, or (at your option)
 any later version.
 .
 On Debian systems, the complete text of version 1 of the GNU General
 Public License can be found in `/usr/share/common-licenses/GPL-1'.
